<?php

require_once('CRM/Report/Form.php');
require_once('CRM/Utils/Type.php');

class CRM_Report_Form_CDNTaxReceipt_ReceiptsIssued extends CRM_Report_Form {

  function __construct() {

    $this->_columns = array(
      'civicrm_contact' =>
      array(
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' =>
        array(
          'sort_name' =>
          array('title' => ts('Contact Name'),
            'required' => TRUE,
          ),
          'id' =>
          array(
            'no_display' => TRUE,
            'required' => TRUE,
          ),
        ),
        'grouping' => 'tax-fields',
        'order_bys' =>
        array(
          'sort_name' =>
          array(
            'title' => ts('Last Name, First Name'), 
          ),
        ),
      ),
      'civicrm_cdntaxreceipts_log' =>
      array(
        'dao' => 'CRM_Contribute_DAO_Contribution',
        'fields' =>
        array(
          'issued_on' => array('title' => 'Issued On', 'default' => TRUE,'type' => CRM_Utils_Type::T_DATE,),
          'receipt_amount' => array('title' => 'Receipt Amount', 'default' => TRUE, 'type' => CRM_Utils_Type::T_MONEY,),
          'receipt_no' => array('title' => 'Receipt No.', 'default' => TRUE),
          'issue_type' => array('title' => 'Issue Type', 'default' => TRUE),
          'issue_method' => array('title' => 'Issue Method', 'default' => TRUE),
        ),
        'grouping' => 'tax-fields',
        'filters' =>
        array(
          'issued_on' =>
          array( 
            'title' => 'Issued On',
            'operatorType' => CRM_Report_Form::OP_DATE),
          'issue_type' =>
          array('title' => ts('Issue Type'),
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => array('single' => 'Single', 'annual' => 'Annual'),
            'type' => CRM_Utils_Type::T_STRING,
          ),
          'issue_method' =>
          array('title' => ts('Issue Method'),
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => array('email' => 'Email', 'print' => 'Print'),
            'type' => CRM_Utils_Type::T_STRING,
          ),
        ),
        'order_bys' =>
        array(
          'issued_on' =>
          array(
            'title' => 'Issued On', 'default' => '1', 'default_weight' => '0', 'default_order' => 'DESC',
          ),
          'receipt_no' =>
          array(
            'title' => ts('Receipt No.'), 
          ),
          'receipt_amount' =>
          array(
            'title' => ts('Receipt Amount'), 
          ),
        ),
      ),
      'civicrm_cdntaxreceipts_log_contributions' =>
      array(
        'dao' => 'CRM_Contribute_DAO_Contribution',
        'fields' =>
        array(
          'contribution_id' => array(
            'default' => TRUE,
            'dbAlias' => "GROUP_CONCAT(DISTINCT cdntaxreceipts_log_contributions_civireport.contribution_id ORDER BY cdntaxreceipts_log_contributions_civireport.contribution_id SEPARATOR ', ')", ),
        ),
        'grouping' => 'tax-fields',
      ),
      'users' =>
      array(
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' =>
        array(
          'name' =>
          array('title' => ts('Issued By'),
            'default' => TRUE,
          ),
        ),
        'grouping' => 'tax-fields',
        'filters' =>
        array(
          'name' =>
          array('title' => ts('Issued By'),
            'type' => CRM_Utils_Type::T_STRING,
          ),
        ),
      ),
    );

    parent::__construct();
  }

  function preProcess() {
    parent::preProcess();

    //check for permission to edit contributions
    if ( ! CRM_Core_Permission::check('access CiviContribute') ) {
      CRM_Core_Error::fatal(ts('You do not have permission to access this page'));
    }
  }

  function select() {
    $select = array();
    $this->_columnHeaders = array();
    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('fields', $table)) {
        foreach ($table['fields'] as $fieldName => $field) {
          if (CRM_Utils_Array::value('required', $field) ||
            CRM_Utils_Array::value($fieldName, $this->_params['fields'])
          ) {
            $alias = "{$tableName}_{$fieldName}";
            $select[] = "{$field['dbAlias']} as {$alias}";
            $this->_columnHeaders["{$tableName}_{$fieldName}"]['type'] = CRM_Utils_Array::value('type', $field);
            $this->_columnHeaders["{$tableName}_{$fieldName}"]['title'] = $field['title'];
            $this->_selectAliases[] = $alias;
          }
        }
      }
    }

    $this->_select = "SELECT " . implode(', ', $select) . " ";
  }

  static
  function formRule($fields, $files, $self) {
    $errors = $grouping = array();
    return $errors;
  }

  function from() {
    $this->_from = "
        FROM {civicrm_cdntaxreceipts_log} {$this->_aliases['civicrm_cdntaxreceipts_log']} 
        INNER JOIN {civicrm_cdntaxreceipts_log_contributions} {$this->_aliases['civicrm_cdntaxreceipts_log_contributions']} 
                ON {$this->_aliases['civicrm_cdntaxreceipts_log']}.id = {$this->_aliases['civicrm_cdntaxreceipts_log_contributions']}.receipt_id 
        LEFT  JOIN {users} {$this->_aliases['users']}
                ON {$this->_aliases['users']}.uid = {$this->_aliases['civicrm_cdntaxreceipts_log']}.uid
        LEFT  JOIN {civicrm_contact} {$this->_aliases['civicrm_contact']}
                ON {$this->_aliases['civicrm_contact']}.id = {$this->_aliases['civicrm_cdntaxreceipts_log']}.contact_id ";  

  }

  function where() {
    parent::where();
    $this->_where .= " AND {$this->_aliases['civicrm_cdntaxreceipts_log']}.is_duplicate = 0 ";
  }


  function groupBy( ) {
    // required for GROUP_CONCAT
    $this->_groupBy = "GROUP BY {$this->_aliases['civicrm_cdntaxreceipts_log']}.id";
  }

  function buildRows($sql, &$rows) {

    // override buildRows() to issue the query through the drupal API
    // rather than through CiviCRM (Drupal allows us to do cross-database
    // querys using the 'db_prefix' in settings.php)

    $query = db_query($sql);

    if (!is_array($rows)) {
      $rows = array();
    }
    
    // use this method to modify $this->_columnHeaders
    $this->modifyColumnHeaders();
    
    $unselectedSectionColumns = $this->unselectedSectionColumns();
    
    while ($result = $query->fetchObject()) {
      $row = array();
      foreach ($this->_columnHeaders as $key => $value) {
        if (property_exists($result, $key)) {
          $row[$key] = $result->$key;
        }
      }
      
      // section headers not selected for display need to be added to row
      foreach ($unselectedSectionColumns as $key => $values) {
        if (property_exists($result, $key)) {
          $row[$key] = $result->$key;
        }
      }
     
      $rows[] = $row;
    }
  }

  function setPager($rowCount = self::ROW_COUNT_LIMIT) {

    // override buildRows() to issue the query through the drupal API
    // rather than through CiviCRM (Drupal allows us to do cross-database
    // querys using the 'db_prefix' in settings.php)

    if ($this->_limit && ($this->_limit != '')) {
      $sql              = "SELECT FOUND_ROWS() as rowcount;";
      $query = db_query($sql);
      $this->_rowsFound = $query->fetchObject()->rowcount;
      $params           = array(
        'total' => $this->_rowsFound,
        'rowCount' => $rowCount,
        'status' => ts('Records') . ' %%StatusMessage%%',
        'buttonBottom' => 'PagerBottomButton',
        'buttonTop' => 'PagerTopButton',
        'pageID' => $this->get(CRM_Utils_Pager::PAGE_ID),
      );
      
      $pager = new CRM_Utils_Pager($params);
      $this->assign_by_ref('pager', $pager);
    }
  }
      
  function postProcess() {

    $this->beginPostProcess();

    $sql = $this->buildQuery(TRUE);

    $rows = $graphRows = array();
    $this->buildRows($sql, $rows);

    $this->formatDisplay($rows);
    $this->doTemplateAssignment($rows);
    $this->endPostProcess($rows);
  }

  function alterDisplay(&$rows) {
    // custom code to alter rows
    $entryFound = FALSE;
    foreach ($rows as $rowNum => $row) {

      // change contact name with link
      if (array_key_exists('civicrm_contact_sort_name', $row) &&
        array_key_exists('civicrm_contact_id', $row)
      ) {
        require_once('CRM/Utils/System.php');
        $url = CRM_Utils_System::url("civicrm/contact/view",
                  'reset=1&cid=' . $row['civicrm_contact_id'],
                  $this->_absoluteUrl
               );
        $rows[$rowNum]['civicrm_contact_sort_name_link'] = $url;
        $rows[$rowNum]['civicrm_contact_sort_name_hover'] = ts("View Contact Summary for this Contact");
        $entryFound = TRUE;
      }

      if (array_key_exists('civicrm_cdntaxreceipts_log_issue_type', $row)) {
        if ($rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_type'] == 'single' ) {
          $rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_type'] = 'Single';
        }
        elseif ($rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_type'] == 'annual' ) {
          $rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_type'] = 'Annual';
        }
        $entryFound = TRUE;
      }

      if (array_key_exists('civicrm_cdntaxreceipts_log_issue_method', $row)) {
        if ($rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_method'] == 'print' ) {
          $rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_method'] = 'Print';
        }
        elseif ($rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_method'] == 'email' ) {
          $rows[$rowNum]['civicrm_cdntaxreceipts_log_issue_method'] = 'Email';
        }
        $entryFound = TRUE;
      }

      if (array_key_exists('civicrm_cdntaxreceipts_log_issued_on', $row)) {
        $rows[$rowNum]['civicrm_cdntaxreceipts_log_issued_on'] = date('Y-m-d', $rows[$rowNum]['civicrm_cdntaxreceipts_log_issued_on']);
        $entryFound = TRUE;
      }

      // skip looking further in rows, if first row itself doesn't
      // have the column we need
      if (!$entryFound) {
        break;
      }
    }
  }
}

