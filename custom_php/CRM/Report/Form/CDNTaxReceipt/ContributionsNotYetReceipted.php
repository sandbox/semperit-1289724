<?php

require_once('CRM/Report/Form.php');
require_once('CRM/Contribute/PseudoConstant.php');
require_once('CRM/Utils/Type.php');
require_once('CRM/Utils/Array.php');

class CRM_Report_Form_CDNTaxReceipt_ContributionsNotYetReceipted extends CRM_Report_Form {

  function __construct() {

    $this->_columns = array(
      'civicrm_contact' =>
      array(
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' =>
        array(
          'sort_name' =>
          array('title' => ts('Contact Name'),
            'required' => TRUE,
          ),
          'id' =>
          array(
            'no_display' => TRUE,
            'required' => TRUE,
          ),
        ),
        'grouping' => 'tax-fields',
        'order_bys' =>
        array(
          'sort_name' =>
          array(
            'title' => ts('Last Name, First Name'), 
          ),
        ),
      ),
      'civicrm_contribution_type' =>
      array(
        'dao' => 'CRM_Contribute_DAO_ContributionType',
        'fields' =>
        array(
          'contribution_type' => array('default' => TRUE),
        ),
        'grouping' => 'tax-fields',
      ),
      'civicrm_contribution' =>
      array(
        'dao' => 'CRM_Contribute_DAO_Contribution',
        'fields' =>
        array(
          'receive_date' => array('default' => TRUE),
          'total_amount' => array('default' => TRUE),
          'contribution_source' => array('default' => TRUE),
          'id' => array('title' => 'Contribution ID', 'default' => TRUE),
        ),
        'filters' =>
        array(
          'receive_date' =>
          array( 
            'operatorType' => CRM_Report_Form::OP_DATE),
          'contribution_type_id' =>
          array('title' => ts('Contribution Type'),
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Contribute_PseudoConstant::contributionType(),
            'type' => CRM_Utils_Type::T_INT,
          ),
        ),
        'grouping' => 'tax-fields',
        'order_bys' =>
        array(
          'total_amount' => NULL,
          'receive_date' =>
          array(
            'default' => '1', 'default_weight' => '0', 'default_order' => 'DESC',
          ),
        ),
      ),
    );

    parent::__construct();
  }

  function preProcess() {
    parent::preProcess();

    //check for permission to edit contributions
    if ( ! CRM_Core_Permission::check('access CiviContribute') ) {
      require_once('CRM/Core/Error.php');
      CRM_Core_Error::fatal(ts('You do not have permission to access this page'));
    }
  }

  function select() {
    $select = array();
    $this->_columnHeaders = array();
    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('fields', $table)) {
        foreach ($table['fields'] as $fieldName => $field) {
          if (CRM_Utils_Array::value('required', $field) ||
            CRM_Utils_Array::value($fieldName, $this->_params['fields'])
          ) {
            $alias = "{$tableName}_{$fieldName}";
            $select[] = "{$field['dbAlias']} as {$alias}";
            $this->_columnHeaders["{$tableName}_{$fieldName}"]['type'] = CRM_Utils_Array::value('type', $field);
            $this->_columnHeaders["{$tableName}_{$fieldName}"]['title'] = $field['title'];
            $this->_selectAliases[] = $alias;
          }
        }
      }
    }

    $this->_select = "SELECT " . implode(', ', $select) . " ";
  }

  static
  function formRule($fields, $files, $self) {
    $errors = $grouping = array();
    return $errors;
  }

  function from() {

    $additionalTables = civicrm_cdntaxreceipts_eligibleForReceiptSQL('from');

    $this->_from = "
        FROM {civicrm_contact} {$this->_aliases['civicrm_contact']}
        INNER JOIN {civicrm_contribution} {$this->_aliases['civicrm_contribution']} 
                ON {$this->_aliases['civicrm_contact']}.id = {$this->_aliases['civicrm_contribution']}.contact_id AND
                   {$this->_aliases['civicrm_contribution']}.is_test = 0
        LEFT  JOIN {civicrm_contribution_type} {$this->_aliases['civicrm_contribution_type']} 
                ON {$this->_aliases['civicrm_contribution']}.contribution_type_id ={$this->_aliases['civicrm_contribution_type']}.id 
        LEFT  JOIN {civicrm_cdntaxreceipts_log_contributions} cdntax_c
                ON {$this->_aliases['civicrm_contribution']}.id = cdntax_c.contribution_id ";
    
    foreach ( $additionalTables as $table ) {
      $this->_from .= "LEFT JOIN {" . $table['name'] . "} " . $table['alias'] .
        " ON " . $table['alias'] . ".entity_id = {$this->_aliases['civicrm_contribution']}.id";
    }

  }

  function where() {

    parent::where();
    
    $eligibility = civicrm_cdntaxreceipts_eligibleForReceiptSQL('where');
    foreach ( $eligibility as $clause ) {
      $this->_where .= " AND "  . $clause;
    }

    $this->_where .= " AND cdntax_c.id IS NULL AND {$this->_aliases['civicrm_contact']}.is_deleted = 0 ";

  }

  function buildRows($sql, &$rows) {

    // override buildRows() to issue the query through the drupal API
    // rather than through CiviCRM (Drupal allows us to do cross-database
    // querys using the 'db_prefix' in settings.php)

    $query = db_query($sql);

    if (!is_array($rows)) {
      $rows = array();
    }
    
    // use this method to modify $this->_columnHeaders
    $this->modifyColumnHeaders();
    
    $unselectedSectionColumns = $this->unselectedSectionColumns();
    
    while ($result = $query->fetchObject()) {
      $row = array();
      foreach ($this->_columnHeaders as $key => $value) {
        if (property_exists($result, $key)) {
          $row[$key] = $result->$key;
        }
      }
      
      // section headers not selected for display need to be added to row
      foreach ($unselectedSectionColumns as $key => $values) {
        if (property_exists($result, $key)) {
          $row[$key] = $result->$key;
        }
      }
     
      $rows[] = $row;
    }
  }

  function setPager($rowCount = self::ROW_COUNT_LIMIT) {
  
    // override buildRows() to issue the query through the drupal API
    // rather than through CiviCRM (Drupal allows us to do cross-database
    // querys using the 'db_prefix' in settings.php)
  
    if ($this->_limit && ($this->_limit != '')) {
      require_once('CRM/Utils/Pager.php');
      
      $sql              = "SELECT FOUND_ROWS() as rowcount;";
      $query = db_query($sql);
      $this->_rowsFound = $query->fetchObject()->rowcount;
      $params           = array(
        'total' => $this->_rowsFound,
        'rowCount' => $rowCount,
        'status' => ts('Records') . ' %%StatusMessage%%',
        'buttonBottom' => 'PagerBottomButton',
        'buttonTop' => 'PagerTopButton',
        'pageID' => $this->get(CRM_Utils_Pager::PAGE_ID),
      );
    
      $pager = new CRM_Utils_Pager($params);
      $this->assign_by_ref('pager', $pager);
    }
  }

  function postProcess() {

    $this->beginPostProcess();

    $sql = $this->buildQuery(TRUE);

    $rows = $graphRows = array();
    $this->buildRows($sql, $rows);

    $this->formatDisplay($rows);
    $this->doTemplateAssignment($rows);
    $this->endPostProcess($rows);
  }

  function alterDisplay(&$rows) {
    // custom code to alter rows
    $entryFound = FALSE;
    foreach ($rows as $rowNum => $row) {

      // change contact name with link
      if (array_key_exists('civicrm_contact_sort_name', $row) &&
        array_key_exists('civicrm_contact_id', $row)
      ) {
        $url = CRM_Utils_System::url("civicrm/contact/view",
                  'reset=1&cid=' . $row['civicrm_contact_id'],
                  $this->_absoluteUrl
               );
        $rows[$rowNum]['civicrm_contact_sort_name_link'] = $url;
        $rows[$rowNum]['civicrm_contact_sort_name_hover'] = ts("View Contact Summary for this Contact");
        $entryFound = TRUE;
      }

      if (array_key_exists('civicrm_contribution_id', $row)) {
        $url = CRM_Utils_System::url("civicrm/contact/view/contribution",
                  'reset=1&id=' . $row['civicrm_contribution_id'] . '&cid=' . $row['civicrm_contact_id'] . '&action=view&context=contribution&selectedChild=contribute',
                  $this->_absoluteUrl
               );
        $rows[$rowNum]['civicrm_contribution_id_link'] = $url;
        $rows[$rowNum]['civicrm_contribution_id_hover'] = ts("View Details of this Contribution");
        $entryFound = TRUE;
      }

      // skip looking further in rows, if first row itself doesn't
      // have the column we need
      if (!$entryFound) {
        break;
      }
    }
  }
}

