<?php
/*
 * Admin configuration form
 *
 */

function civicrm_cdntaxreceipts_admin_form() {

  $form = array('#attributes' => array('enctype' => "multipart/form-data"));

  $form['help'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tip'),
    '#description' => t('After you fill out this form and save your Configuration, create a fake Donation in CiviCRM and issue a Tax Receipt for it to check the graphics/layout of the Tax Receipt that is generated. If necessary - rework your graphics and come back to this Form to upload the new version(s).'),
  );

  $form['org'] = array(
    '#type' => 'fieldset',
    '#title' => t('Organization details'),
  );
  $form['org']['cdntaxreceipts_org_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization Name'),
    '#size' => 50,
    '#default_value' => variable_get('cdntaxreceipts_org_name', ''),
    '#maxlength' => 50,
    '#required' => TRUE,
  );
  $form['org']['cdntaxreceipts_address_line1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 1'),
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => variable_get('cdntaxreceipts_address_line1', ''),
    '#description' => '101 Anywhere Drive',
    '#required' => TRUE,
  );
  $form['org']['cdntaxreceipts_address_line2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 2'),
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => variable_get('cdntaxreceipts_address_line2', ''),
    '#description' => 'Toronto ON A1B 2C3',
    '#required' => TRUE,
  );
  $form['org']['cdntaxreceipts_tel'] = array(
    '#type' => 'textfield',
    '#title' => t('Telephone'),
    '#size' => 16,
    '#maxlength' => 16,
    '#default_value' => variable_get('cdntaxreceipts_tel', ''),
    '#description' => '(555) 555-5555',
    '#required' => TRUE,
  );
  $form['org']['cdntaxreceipts_fax'] = array(
    '#type' => 'textfield',
    '#title' => t('Fax'),
    '#size' => 16,
    '#maxlength' => 16,
    '#default_value' => variable_get('cdntaxreceipts_fax', ''),
    '#description' => '(555) 555-5555',
    '#required' => FALSE,
  );
  $form['org']['cdntaxreceipts_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' => 40,
    '#maxlength' => 40,
    '#default_value' => variable_get('cdntaxreceipts_email', ''),
    '#description' => 'info@my.org',
    '#required' => TRUE,
  );
  $form['org']['cdntaxreceipts_web'] = array(
    '#type' => 'textfield',
    '#title' => t('Website'),
    '#size' => 40,
    '#maxlength' => 40,
    '#default_value' => variable_get('cdntaxreceipts_web', ''),
    '#description' => 'www.my.org',
    '#required' => TRUE,
  );
  $form['org']['cdntaxreceipts_charitable_no'] = array(
    '#type' => 'textfield',
    '#title' => t('Charitable Registration Number'),
    '#size' => 20,
    '#maxlength' => 20,
    '#default_value' => variable_get('cdntaxreceipts_charitable_no', ''),
    '#description' => '10000-000-RR0000',
    '#required' => TRUE,
  );
  $form['org']['cdntaxreceipts_authorized_signature_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorized Signature Text'),
    '#size' => 60,
    '#maxlength' => 60,
    '#default_value' => variable_get('cdntaxreceipts_authorized_signature_text', ''),
    '#description' => 'Name and position of the authorizing official, defaults to Authorized Signature',
    '#required' => FALSE,
  );

  $form['receipt'] = array(
    '#type' => 'fieldset',
    '#title' => t('Receipt configuration'),
  );
  $form['receipt']['cdntaxreceipts_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Receipt Prefix'),
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => variable_get('cdntaxreceipts_prefix', 'WEB-'),
    '#description' => t('Receipt numbers are formed by appending the CiviCRM Contribution ID to this prefix. Receipt numbers must be unique within your organization.'),
    '#required' => TRUE,
  );

  $form['receipt']['cdntaxreceipts_logo'] = array(
    '#type' => 'file',
    '#title' => t('Logo'),
    '#size' => 60,
    '#description' => 'Logo size: 280x120 pixels; File types allowed: .jpg .png - Currently using: ' . variable_get('cdntaxreceipts_logo', ''),
  );
  $form['receipt']['cdntaxreceipts_signature'] = array(
    '#type' => 'file',
    '#title' => t('Signature'),
    '#size' => 60,
    '#description' => 'Signature size: 141x58 pixels; File types allowed: .jpg .png - Currently using: ' . variable_get('cdntaxreceipts_signature', ''),
  );
  $form['receipt']['cdntaxreceipts_bg'] = array(
    '#type' => 'file',
    '#title' => t('Background Image'),
    '#size' => 60,
    '#description' => 'Watermark Image size: 250x250 pixels; File types allowed: .jpg .png - Currently using: ' . variable_get('cdntaxreceipts_bg', ''),
  );

  // System Configuration

  $form['system'] = array(
    '#type' => 'fieldset',
    '#title' => t('System and PDF options'),
  );
  $form['system']['cdntaxreceipts_issue_inkind'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set up In-kind receipts?'),
    '#default_value' => variable_get('cdntaxreceipts_issue_inkind', FALSE),
    '#description' => t('Checking this box will set up the fields required to generate in-kind tax receipts. Unchecking the box will not disable in-kind receipts: you will need to do that manually, by disabling the In-kind contribution type or making it non-deductible in the CiviCRM administration pages.'),
  );
  $form['system']['cdntaxreceipts_enable_email'] = array(
    '#type' => 'radios',
    '#title' => t('Send receipts by email?'),
    '#default_value' => variable_get('cdntaxreceipts_enable_email', TRUE),
    '#options' => array( TRUE => 'Yes', FALSE => 'No' ),
    '#description' => t('If enabled, tax receipts will be sent via email to donors who have an email address on file.'),
  );
  
  $pdf_tools = _civicrm_cdntaxreceipts_tools();
  if (is_array($pdf_tools)) {
    $form['system']['cdntaxreceipts_pdf_tool'] = array(
        '#type' => 'radios',
        '#title' => t('PDF generation tool'),
        '#options' => $pdf_tools,
        '#default_value' => variable_get('cdntaxreceipts_pdf_tool', CIVICRM_CDNTAXRECEIPTS_PDF_TOOL_DEFAULT),
        '#description' => t('This option selects the PDF generation tool being used by this module to create the PDF output.'),
      );
  }
  else {
    $form['system']['cdntaxreceipts_pdf_tool'] = array(
        '#type' => 'markup',
        '#title' => t('PDF generation tool'),
        '#value' => 'No tcpdf library found. Please check your install.',
        '#description' => t('This option selects the PDF generation tool being used by this module to create the PDF output.'),
      );    
  }

  // Create an Email message fieldset to gather info about the Email message 
  // to accompany the Tax Receipt

  $form['message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email message'),
  );
  $form['message']['cdntaxreceipts_subject'] = array(
   '#type' => 'textfield',
   '#title' => t('Email Subject'),
   '#size' => 50,
   '#maxlength' => 50,
   '#default_value' => variable_get('cdntaxreceipts_subject', 'Your Tax Receipt'),
   '#description' => t('Subject of the Email to accompany your Tax Receipt. The receipt number will be appended.'),
   '#required' => TRUE,
  );
  $form['message']['cdntaxreceipts_from'] = array(
   '#type' => 'textfield',
   '#title' => t('Email From'),
   '#size' => 50,
   '#maxlength' => 50,
   '#default_value' => variable_get('cdntaxreceipts_from', ''),
   '#description' => t('Address you would like to Email the Tax Receipt from.'),
   '#required' => TRUE,
  );
  $form['message']['cdntaxreceipts_archive'] = array(
   '#type' => 'textfield',
   '#title' => t('Archive Email'),
   '#size' => 50,
   '#maxlength' => 50,
   '#default_value' => variable_get('cdntaxreceipts_archive', ''),
   '#description' => t('Address you would like to Send a copy of the Email containing the Tax Receipt to. This is useful to create an archive.'),
   '#required' => TRUE,
  );
  $form['message']['cdntaxreceipts_message'] = array(
   '#type' => 'textarea',
   '#title' => t('Email Message'),
   '#size' => 1500,
   '#maxlength' => 1500,
   '#default_value' => variable_get('cdntaxreceipts_message', 'Attached please find your official tax receipt for income tax purposes'),
   '#description' => t('Text in the Email to accompany your Tax Receipt.'),
   '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

function civicrm_cdntaxreceipts_admin_form_submit($form, &$form_state) {

  $file_keys = array('cdntaxreceipts_logo','cdntaxreceipts_signature','cdntaxreceipts_bg');
  // save non-file values as Drupal variables
  foreach ($form_state['values'] as $key => $value) {
    if (!in_array($key,$file_keys)) {
      variable_set($key, $value);
    }
  }

  // check whether I need to  set up custom fields for in-kind data
  if ($form_state['values']['cdntaxreceipts_issue_inkind'] === 1) {
    // requires complicated civicrm-specific code 
    module_load_include('inc', 'civicrm_cdntaxreceipts', 'civicrm_cdntaxreceipts');
    civicrm_cdntaxreceipts_configure_inkind_fields();
  }

  // save logo, signature and background (watermark) files
  foreach ($file_keys as $key ) {
    if (is_uploaded_file( $_FILES['files']['tmp_name'][$key])) {
      $mypath = conf_path() . '/files';
      $directory = variable_get('file_system_path', $mypath);
      $validators = array('file_validate_is_image' => array());

      // D6: $file = file_save_upload( $key, $validators, $directory, FILE_EXISTS_REPLACE );
      $file = file_save_upload( $key, $validators, 'public://', FILE_EXISTS_REPLACE );

      if ( $file ) {
        // D6: file_set_status( $file, FILE_STATUS_PERMANENT );
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        $filepath = $directory . '/' . $file->filename;
        variable_set($key, $filepath);
        drupal_set_message(t('Uploaded file to %fp', array('%fp' => $filepath)));
      }
      else {
        drupal_set_message(t('Error uploading the file.'));
      }
    }
  }
}

/**
 * Auxiliary function to locate suitable PDF generation tools
 *
 * @return
 *   array of filenames with the include-able PHP file of the located tools
 */
function _civicrm_cdntaxreceipts_tools() {
  $pattern = '/^tcpdf.php$/';
  $tools = array_keys(file_scan_directory(drupal_get_path('module', 'civicrm_cdntaxreceipts'), $pattern));
  $tools = array_merge($tools, array_keys(file_scan_directory(CIVICRM_CDNTAXRECEIPTS_LIB_PATH, $pattern)));
  if (module_exists('libraries')) {
    $tools = array_merge($tools, array_keys(file_scan_directory(libraries_get_path('tcpdf'), '/^tcpdf.php$/')));
  }

  $num_tools = count($tools);

  if ($num_tools == 0) {
    variable_set('cdntaxreceipts_pdf_tool', CIVICRM_CDNTAXRECEIPTS_PDF_TOOL_DEFAULT);
    return -1;
  }
  else {
    return array_combine($tools, $tools);
  }
}

