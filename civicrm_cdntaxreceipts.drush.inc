<?php

/**
 * @file
 * This file contains drush integration for the civicrm_cdntaxreceipts module.
 */

/**
 * Implements hook_drush_command().
 */
function civicrm_cdntaxreceipts_drush_command() {
  $items = array();
  $items['cctr-status'] = array(
    'description' => 'Output the current module status',
    'drupal dependencies' => array('civicrm_cdntaxreceipts', 'civicrm'),
    'callback' => 'civicrm_cdntaxreceipts_status',
    'examples' => array(
      'drush cctr-status' =>
        'Output the current status of the cdn tax receipts module',
    ),
  );

  return $items;
}

/**
 * Callback for cctr-status drush command.
 */
function civicrm_cdntaxreceipts_status() {
  return 'Placeholder';  
}

